-- Science Fusion NA 6-8 2017 counts

--  https://www-k6.thinkcentral.com/
-- Evaluators click here
-- Email: sarah.lee@hmhco.com (registered using access word k8fusion17)
-- Click login beside ScienceFusion National 2017 K-8

SELECT
    drwn_a2ppoc.tck6_akamai.dt AS Month,
    'NA' AS Standard,
    '6-8' AS Grade,
    regexp_extract(drwn_a2ppoc.tck6_akamai.resource, '_([0-9]{13})_', 1) AS ISBN,
    regexp_replace(drwn_a2ppoc.tck6_akamai.role, '\W*((?i)Student,(?-i))\W*', '') AS role,
    drwn_a2ppoc.tck6_akamai.status_code,
    drwn_a2ppoc.tck6_akamai.request,
    drwn_a2ppoc.tck6_akamai.resource,
    drwn_a2ppoc.tck6_akamai.query_param AS qparam,
    drwn_a2ppoc.tck6_akamai.referrer,
    drwn_a2ppoc.tck6_akamai.browser AS user_agent,
    drwn_a2ppoc.tck6_akamai.ip,
    drwn_a2ppoc.tck6_akamai.ip_num,
    drwn_a2ppoc.tck6_akamai.cn AS CName,
    drwn_a2ppoc.tck6_akamai.uid,
    drwn_a2ppoc.tck6_akamai.uniqueid,
    drwn_a2ppoc.tck6_akamai.org AS organisation,
    drwn_a2ppoc.tck6_akamai.dc AS district_pid,
    drwn_a2ppoc.tck6_akamai.dist_refid AS dist_ref_id,
    drwn_a2ppoc.tck6_akamai.state,
    drwn_a2ppoc.tck6_akamai.country
FROM drwn_a2ppoc.tck6_akamai
WHERE resource in
-- URL HMD SCIENCE FUSION NATIONAL 6-8 2017
(
-- Teacher Online Books
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grma/ete_9780544780583_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmb/ete_9780544780590_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmc/ete_9780544780606_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmd/ete_9780544780613_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grme/ete_9780544780620_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmf/ete_9780544780637_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmg/ete_9780544780644_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmh/ete_9780544780651_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmi/ete_9780544780668_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmj/ete_9780544780675_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmk/ete_9780544780682_/index.html?page=i',
-- Student Online Books
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grma/ese_9780544780415_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmb/ese_9780544780422_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmc/ese_9780544780439_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmd/ese_9780544780446_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grme/ese_9780544780453_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmf/ese_9780544780460_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmg/ese_9780544780477_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmh/ese_9780544780484_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmi/ese_9780544780491_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmj/ese_9780544780507_/index.html?page=i',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmk/ese_9780544780514_/index.html?page=i',
-- Teacher Gateways
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grma/teacher_gateway_9780544863705_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmb/teacher_gateway_9780544863712_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmc/teacher_gateway_9780544863729_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmd/teacher_gateway_9780544863736_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grme/teacher_gateway_9780544863743_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmf/teacher_gateway_9780544863750_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmg/teacher_gateway_9780544863767_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmh/teacher_gateway_9780544863774_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmi/teacher_gateway_9780544863781_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmj/teacher_gateway_9780544863798_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmk/teacher_gateway_9780544863804_/index.html',
-- Student Gateways
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grma/student_gateway_9780544860780_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmb/student_gateway_9780544860797_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmc/student_gateway_9780544860803_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmd/student_gateway_9780544860810_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grme/student_gateway_9780544860827_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmf/student_gateway_9780544860834_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmg/student_gateway_9780544860841_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmh/student_gateway_9780544860858_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmi/student_gateway_9780544860865_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmj/student_gateway_9780544860872_/index.html',
'/www-k6.thinkcentral.com/content/hsp/science/fusion2017/na/grmk/student_gateway_9780544860889_/index.html',
)
AND drwn_a2ppoc.tck6_akamai.status_code < 400
