-- 20 Aug 2018 Total Collections. Count = ?
-- This is a work in progress - all grades and Interactive Lessons for natl, NA and CA  need to be added 

-- We are NOT deriving each DLO ISBN (will need to use the 'coalesce' presto command) for now

SELECT drwn_a2ppoc.hmof_akamai.Date,
    drwn_a2ppoc.hmof_akamai.dt AS Month,
    regexp_extract(drwn_a2ppoc.hmof_akamai.resource, '\/([a-zA-Z]+)\/gr', 1) AS Standard,
    regexp_extract(drwn_a2ppoc.hmof_akamai.resource, '\/gr([0-9]+)\/', 1) AS Grade,
    regexp_extract(drwn_a2ppoc.hmof_akamai.resource, '_([0-9]+)_\/index.html', 1) AS ISBN,
    regexp_replace(drwn_a2ppoc.hmof_akamai.role, '\W*((?i)Student,(?-i))\W*', '') AS role,
    drwn_a2ppoc.hmof_akamai.request,
    drwn_a2ppoc.hmof_akamai.resource,
    drwn_a2ppoc.hmof_akamai.query_param AS qparam,
    drwn_a2ppoc.hmof_akamai.referrer,
    drwn_a2ppoc.hmof_akamai.status_code,
    drwn_a2ppoc.hmof_akamai.browser AS user_agent,
    drwn_a2ppoc.hmof_akamai.ip,
    drwn_a2ppoc.hmof_akamai.ip_num,
    drwn_a2ppoc.hmof_akamai.cn AS CName,
    drwn_a2ppoc.hmof_akamai.uid,
    drwn_a2ppoc.hmof_akamai.uniqueid,
    drwn_a2ppoc.hmof_akamai.userrefid,
    drwn_a2ppoc.hmof_akamai.org AS organisation,
    drwn_a2ppoc.hmof_akamai.dc AS district_pid,
    drwn_a2ppoc.hmof_akamai.dist_refid,
    drwn_a2ppoc.hmof_akamai.state,
    drwn_a2ppoc.hmof_akamai.country
 FROM drwn_a2ppoc.hmof_akamai
WHERE resource = '/my.hrw.com/content/hmof/language_arts/hmhcollections2017/na/gr7/ete_9780544570818_/index.html'
   OR resource = '/my.hrw.com/content/hmof/language_arts/hmhcollections2017/na/gr6/ete_9780544570801_/index.html'
   OR resource = '/my.hrw.com/content/hmof/language_arts/hmhcollections2017/natl/gr6/dlo_landing_page/Teacher_Landing_Page/index.html'

-- LIMIT 20
