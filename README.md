# Darwinator Project - SQL Repository
## What is this repository for?
A collection of all Athena SQL queries for the Darwinator project.
Version: 1.0

## Architecture
All Akamai logs for both TC and HRW are stored in S3 as parquet files, partionned by month. Athena is used to query those logs and extract useful sub-datasets to then be ingexted by a Postgres SQL databse that's the heart of the Metabase BI visualisation tool.
### Athena DB setup
The following tables are on Athena under the drwn_a2ppoc database:


- `hmof_akamai`: Akamai logs for HMOF
- `tck6_akamai`: Akamai logs for TC
- `hrw_eproduct_bundle_detail`: HMOF bundle details including TE/SE ISBNs as well as secure online ISBNs
- `tc_isbns`: TC ISBN details including TE/SE ISBNs
- `organisations`: list of organisations including District ID/School ID for both platforms TC and HMOF
- `org_ent`: Org entitlements for TC

### Query Format for "usage statistics"
Since all queries are derived from Athena's drwn_a2ppoc.hmof_akamai table, they must all generate the same output. The only diffrence between queries should be the way to extract ISBNs, Standards and Grades and the Launch URL condition. For unknown/un-retrievable fields, use `NULL` as value instead of `N/A` or other text values.

The query format should be as follow:


```sql
SELECT drwn_a2ppoc.hmof_akamai.Date,
    drwn_a2ppoc.hmof_akamai.dt AS Month,
    UPPER(regexp_extract(`expression`)) AS Standard,
    regexp_extract(`expression`) AS Grade,
    coalesce(regexp_replace(regexp_extract(referrer, ‘isbn=([a-zA-Z0-9+/=]+)‘), ‘[^0-9]‘, ‘’), regexp_replace(regexp_extract(query_param, ‘isbn=([a-zA-Z0-9+/=]+)‘), ‘[^0-9]‘, ‘’)) 
        AS ISBN
    drwn_a2ppoc.hmof_akamai.request,
    drwn_a2ppoc.hmof_akamai.resource,
    drwn_a2ppoc.hmof_akamai.query_param AS qparam,
    drwn_a2ppoc.hmof_akamai.referrer,
    drwn_a2ppoc.hmof_akamai.status_code,
    drwn_a2ppoc.hmof_akamai.browser AS user_agent,
    drwn_a2ppoc.hmof_akamai.ip,
    drwn_a2ppoc.hmof_akamai.ip_num,
    drwn_a2ppoc.hmof_akamai.cn AS CName,
    drwn_a2ppoc.hmof_akamai.uid,
    drwn_a2ppoc.hmof_akamai.uniqueid,
    drwn_a2ppoc.hmof_akamai.userrefid,
    drwn_a2ppoc.hmof_akamai.org AS organisation,
    drwn_a2ppoc.hmof_akamai.dc AS district_pid,
    drwn_a2ppoc.hmof_akamai.dist_refid,
    drwn_a2ppoc.hmof_akamai.state,
    drwn_a2ppoc.hmof_akamai.country,
    drwn_a2ppoc.hmof_akamai.referrer,
FROM drwn_a2ppoc.hmof_akamai
WHERE `URL_Launch condition`
  -- AND drwn_a2ppoc.hmof_akamai.status_code = 400;
```

### Contribution guidelines


#### Writing tests


#### Code review


#### Other guidelines
#### Who do I talk to?
#### Repo owner or admin
#### Other community or team contact