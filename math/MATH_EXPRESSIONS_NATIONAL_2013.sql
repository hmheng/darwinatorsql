SELECT
    drwn_a2ppoc.tck6_akamai.Date,
    drwn_a2ppoc.tck6_akamai.dt AS Month,
    'na' AS Standard,
    replace(
    regexp_replace(
      regexp_extract(lower(drwn_a2ppoc.tck6_akamai.resource), '/gr(.*?)/', 1),
      '(m.*)', '6-8'), '0', 'K') AS Grade,
    regexp_extract(drwn_a2ppoc.tck6_akamai.resource, '_([0-9]{13})_', 1) AS ISBN,
    regexp_replace(drwn_a2ppoc.tck6_akamai.role, '\W*((?i)Student,(?-i))\W*', '') AS role,
    drwn_a2ppoc.tck6_akamai.status_code,
    drwn_a2ppoc.tck6_akamai.request,
    drwn_a2ppoc.tck6_akamai.resource,
    drwn_a2ppoc.tck6_akamai.query_param AS qparam,
    drwn_a2ppoc.tck6_akamai.referrer,
    drwn_a2ppoc.tck6_akamai.browser AS user_agent,
    drwn_a2ppoc.tck6_akamai.ip,
    drwn_a2ppoc.tck6_akamai.ip_num,
    drwn_a2ppoc.tck6_akamai.cn AS CName,
    drwn_a2ppoc.tck6_akamai.uid,
    drwn_a2ppoc.tck6_akamai.uniqueid,
    drwn_a2ppoc.tck6_akamai.org AS organisation,
    drwn_a2ppoc.tck6_akamai.dc AS district_pid,
    drwn_a2ppoc.tck6_akamai.dist_refid AS dist_ref_id,
    drwn_a2ppoc.tck6_akamai.state,
    drwn_a2ppoc.tck6_akamai.country
FROM drwn_a2ppoc.tck6_akamai
WHERE lower(resource) IN ('/www-k6.thinkcentral.com/content/hsp/math/hspmath/na/gr6/math_expressions_se_9780547662480_/common/startup/intro.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/na/gr6/math_expressions_te_9780547662558_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr2/ete_9780547838724_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr3/ete_9780547838731_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr4/ete_9780547838748_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr1/ete_9780547838717_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr5/ete_9780547838755_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/grk/ete_9780547838779_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr1/ese_9780547838298_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr2/ese_9780547838304_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr3/ese_9780547838311_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr4/ese_9780547838328_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/gr5/ese_9780547838335_/main.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmathmx/na/grk/ese_9780547838359_/main.html'
)
AND drwn_a2ppoc.tck6_akamai.status_code < 400;