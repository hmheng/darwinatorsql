SELECT
    drwn_a2ppoc.tck6_akamai.Date,
    drwn_a2ppoc.tck6_akamai.dt AS Month,
    'na' AS Standard,
    replace(
    regexp_replace(
      regexp_extract(lower(drwn_a2ppoc.tck6_akamai.resource), '/gr(.*?)/', 1),
      '(m.*)', '6-8'), '0', 'K') AS Grade,
    regexp_extract(drwn_a2ppoc.tck6_akamai.resource, '_([0-9]{13})_', 1) AS ISBN,
    regexp_replace(drwn_a2ppoc.tck6_akamai.role, '\W*((?i)Student,(?-i))\W*', '') AS role,
    drwn_a2ppoc.tck6_akamai.status_code,
    drwn_a2ppoc.tck6_akamai.request,
    drwn_a2ppoc.tck6_akamai.resource,
    drwn_a2ppoc.tck6_akamai.query_param AS qparam,
    drwn_a2ppoc.tck6_akamai.referrer,
    drwn_a2ppoc.tck6_akamai.browser AS user_agent,
    drwn_a2ppoc.tck6_akamai.ip,
    drwn_a2ppoc.tck6_akamai.ip_num,
    drwn_a2ppoc.tck6_akamai.cn AS CName,
    drwn_a2ppoc.tck6_akamai.uid,
    drwn_a2ppoc.tck6_akamai.uniqueid,
    drwn_a2ppoc.tck6_akamai.org AS organisation,
    drwn_a2ppoc.tck6_akamai.dc AS district_pid,
    drwn_a2ppoc.tck6_akamai.dist_refid AS dist_ref_id,
    drwn_a2ppoc.tck6_akamai.state,
    drwn_a2ppoc.tck6_akamai.country
FROM drwn_a2ppoc.tck6_akamai
WHERE lower(resource) IN ('/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/grk/se_9780547587721_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr4/se_9780547587868_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr5/se_9780547587684_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr6/se_9780547587882_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr2/se_9780547587752_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr3/se_9780547587875_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/gomath2012/na/gr1/se_9780547587639_/default.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr3/te_9780547587073_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr4/te_9780547587080_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr5/te_9780547587097_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr6/te_9780547587103_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/grk/te_9780547587110_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr1/te_9780547587059_/launch.html',
'/www-k6.thinkcentral.com/content/hsp/math/hspmath/go_math_2012/na/gr2/te_9780547587066_/launch.html'
)
AND drwn_a2ppoc.tck6_akamai.status_code < 400