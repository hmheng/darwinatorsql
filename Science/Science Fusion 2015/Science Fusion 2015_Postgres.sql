-- Create a new schema, and set privileges
CREATE SCHEMA science_fusion_2015;
GRANT USAGE ON SCHEMA science_fusion_2015 TO metabase;
GRANT USAGE ON SCHEMA science_fusion_2015 TO metabase_script;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA science_fusion_2015 TO metabase;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA science_fusion_2015 TO metabase_script;

-- Create the akamai_logs table
CREATE TABLE IF NOT EXISTS postgres.science_fusion_2015.akamai_logs (
  "date" timestamp,
  "month" text,
  "standard" text,
  "grade" text,
  "isbn" text,
  "role" text,
  "status_code" smallint,
  "request" text,
  "resource" text,
  "qparam" text,
  "referrer" text,
  "user_agent" text,
  "ip" inet,
  "ip_num" bigint,
  "cname" text,
  "uid" text,
  "uniqueid" text,
  "organisation" text,
  "district_pid" text,
  "dist_ref_id" text,
  "state" text,
  "country" text);

-- Import data into the akamai_logs table
COPY science_fusion_2015.akamai_logs FROM '/storage/raw_data/Science_fusion_2015.csv' WITH CSV HEADER QUOTE '"' DELIMITER ',';

-- Data manipulation
-- Assign the right standard / grade to the launch URLs that didn't have standard/grade in URL
UPDATE science_fusion_2015.akamai_logs
SET standard = 'ia_sioux_city', grade = '6-8'
WHERE resource in (
  '/www-k6.thinkcentral.com/content/hsp/science/fusion/nm/common/history_channel_videos_9780547705415_/',
  '/www-k6.thinkcentral.com/content/hsp/science/fusion/nm/common/peopleinscience_9780547705422_/index.html',
  '/www-k6.thinkcentral.com/content/hsp/science/fusion/na_agnostic/common/unit_quiz_9780544048720_/');

-- a few distpids had a TAB character in them, this is how to remove them
UPDATE science_fusion_2015.akamai_logs
SET district_pid = TRIM(REPLACE(district_pid, chr(9), ''))
WHERE strpos(district_pid, chr(9)) > 0;

-- Assign the correct standard to OH ISBNs that were extracted as NA but belong to OH districts
UPDATE science_fusion_2015.akamai_logs A
SET standard = 'oh'
FROM (SELECT DISTINCT uid, role, organisation, district_pid FROM science_fusion_2015.akamai_logs WHERE standard = 'oh') B
WHERE A.uid = B.uid
  AND A.role = B.role
  AND A.organisation = B.organisation
  AND A.resource IN (
    '/www-k6.thinkcentral.com/content/hsp/science/fusion/na/gr00/student_gateway_9780547778754_/launch.html',
    '/www-k6.thinkcentral.com/content/hsp/science/fusion/na/gr01/student_gateway_9780547778761_/launch.html',
    '/www-k6.thinkcentral.com/content/hsp/science/fusion/na/gr02/student_gateway_9780547778785_/launch.html');