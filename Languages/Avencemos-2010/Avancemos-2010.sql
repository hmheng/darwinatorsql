-- 15 Aug 2018 Total Count = 708,401
-- Count of 5877 for 'Veiw your Students Work'

SELECT drwn_a2ppoc.hmof_akamai.Date,
    drwn_a2ppoc.hmof_akamai.dt AS Month,
    'NA' AS Standard,
    regexp_extract(drwn_a2ppoc.hmof_akamai.resource, 'av13_([a-zA-Z0-9]+)\.jsp', 1) AS Grade,
    coalesce(
      regexp_replace(
        regexp_extract(drwn_a2ppoc.hmof_akamai.referrer, 'isbn=([a-zA-Z0-9+/=]+)'), '[^0-9]', ''),
        regexp_replace(drwn_a2ppoc.hmof_akamai.query_param, '[^0-9]', '')
      ) AS ISBN,
    regexp_replace(drwn_a2ppoc.hmof_akamai.role, '\W*((?i)Student,(?-i))\W*', '') AS role,
    drwn_a2ppoc.hmof_akamai.request,
    drwn_a2ppoc.hmof_akamai.resource,
    drwn_a2ppoc.hmof_akamai.query_param AS qparam,
    drwn_a2ppoc.hmof_akamai.referrer,
    drwn_a2ppoc.hmof_akamai.status_code,
    drwn_a2ppoc.hmof_akamai.browser AS user_agent,
    drwn_a2ppoc.hmof_akamai.ip,
    drwn_a2ppoc.hmof_akamai.ip_num,
    drwn_a2ppoc.hmof_akamai.cn AS CName,
    drwn_a2ppoc.hmof_akamai.uid,
    drwn_a2ppoc.hmof_akamai.uniqueid,
    drwn_a2ppoc.hmof_akamai.userrefid,
    drwn_a2ppoc.hmof_akamai.org AS organisation,
    drwn_a2ppoc.hmof_akamai.dc AS district_pid,
    drwn_a2ppoc.hmof_akamai.dist_refid,
    drwn_a2ppoc.hmof_akamai.state,
    drwn_a2ppoc.hmof_akamai.country
FROM drwn_a2ppoc.hmof_akamai
WHERE resource LIKE '/my.hrw.com/wl2/wl10/nsmedia/uppernav/upper_%.jsp' 
   OR (resource = '/my.hrw.com/tabnav/spytop.jsp' AND query_param IN ('isbn=0547318677', 
                                                                      'isbn=0547318650', 
                                                                      'isbn=0547318669', 
                                                                      'isbn=0547318685',      
                                                                      'isbn=0547318693',
                                                                      'isbn=0547318707'))